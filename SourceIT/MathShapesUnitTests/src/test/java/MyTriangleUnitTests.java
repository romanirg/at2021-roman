import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MyTriangleUnitTests {

    MyTriangle triangle = new MyTriangle(2.0, 3.0, 4.0);




    @Test
    public void GetA_Expect_A_Equals_2() {
        double expectedA = 2.0;
        double actualA = triangle.getA();
        assertEquals(expectedA, actualA, 0.0);
    }

    @Test
    public void GetB_Expect_B_Equals_3() {
        double expectedB = 3.0;
        double actualB = triangle.getB();
        assertEquals(expectedB, actualB, 0.0);
    }

    @Test
    public void GetC_Expect_C_Equals_4() {
        double expectedC = 4.0;
        double actualC = triangle.getC();
        assertEquals(expectedC, actualC, 0.0);
    }


    @Test
    public void calcSquare_UsingGetA_GetB_GetC_Expect_Pass() {
        double expectedS = 2.9;
        triangle.getA();
        triangle.getB();
        triangle.getC();
        assertEquals(expectedS , triangle.calcSquare() , 0.1);
    }

    @Test
    public void SetA_4_Expect_A_Equals_4() {
        double expectedA = 4.0;
        triangle.setA(4.0);
        assertEquals(expectedA , triangle.a , 0.0);
    }

    @Test
    public void SetB_2_Expect_B_Equals_2() {
        double expectedB = 2.0;
        triangle.setB(2.0);
        assertEquals(expectedB , triangle.b , 0.0);
    }

    @Test
    public void SetC_3_Expect_C_Equals_3() {
        double expectedC = 3.0;
        triangle.setC(3.0);
        assertEquals(expectedC , triangle.c , 0.0);
    }


    @Test
    public void calcSquare_UsingSetA_Expect_Pass() {
        double expectedS = 2.9;
        triangle.setA(4.0);
        triangle.setB(2.0);
        triangle.setC(3);
        assertEquals(expectedS , triangle.calcSquare() , 0.1);
    }

    @Test
    public void triangle_Is_InstanceOf_MyTriangleClass_Expect_Pass() {
        Assert.assertTrue(triangle instanceof MyTriangle);
    }

    @Test
    public void triangle_Is_InstanceOf_MyRectangleClass_Expect_Pass() {
        Assert.assertTrue(triangle instanceof MyRectangle);
    }

    @Test
    public void triangle_Is_InstanceOf_MySquare_Expect_Pass() {
        Assert.assertTrue(triangle instanceof MySquare);
    }
}
