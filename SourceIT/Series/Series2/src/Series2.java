import java.io.BufferedReader;
import java.io.FileReader;



public class Series2 {




    public static void main(String[] args) throws Exception {
        BufferedReader inputFile = new BufferedReader(new FileReader("data.txt"));
        String strtmp;
        int value;

        int sum = 1;


        while ((strtmp = inputFile.readLine()) != null) {
            if (!strtmp.isEmpty())
            {
                value = Integer.parseInt(strtmp);
                sum *= value;
            }
        }
        System.out.println(sum);
        inputFile.close();

    }
}