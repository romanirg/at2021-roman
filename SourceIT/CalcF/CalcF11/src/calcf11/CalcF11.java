package calcf11;
import java.util.Scanner;

public class CalcF11 {
    
    
    
    
    public static boolean firstCondition(int a) 
    {
        if (a % 2 == 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    public static boolean secondCondition(int b) 
    {
        if (b % 2 == 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public static void main(String[] args) {
        
        
        System.out.println("Input number A ");
       
         Scanner in = new Scanner(System.in);
         int x = in.nextInt();
         
         System.out.println("Input number B ");
         
         Scanner in2 = new Scanner(System.in);
         int y = in2.nextInt();
         
         
         
         if (firstCondition(x) == true && secondCondition(y) == true)
         {
             System.out.println("Integers A and B are even");
         }
         if (firstCondition(x) == false && secondCondition(y) == false)
         {
             System.out.println("Integers A and B are odd");
         }
         if (firstCondition(x) == false && secondCondition(y) == true)
         {
             System.out.println("The parity of numbers A and B does not match");
         }
         if (firstCondition(x) == true && secondCondition(y) == false)
         {
             System.out.println("The parity of numbers A and B does not match");
         }
               
         
        in.close();

    }

}
