import java.io.BufferedReader;
import java.io.FileReader;



public class Series5 {




    public static void main(String[] args) throws Exception {
        BufferedReader inputFile = new BufferedReader(new FileReader("data.txt"));
        String strtmp;
        double value;
        double product = 1.0;
        double factor;


        while ((strtmp = inputFile.readLine()) != null) {

                value = Double.parseDouble(strtmp);
                factor = Math.floor(value);
                product *= factor;

            }




        System.out.println("The arithmetic mean for the given set of numbers equals " + product);
        inputFile.close();

    }

}

