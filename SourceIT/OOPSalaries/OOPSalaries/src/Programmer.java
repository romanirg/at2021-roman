public class Programmer extends Person implements Employee {
    private int hours;
    private int rate;

    public Programmer(String name, int age, String gender, int id, String phoneNumber, String email, int hours, int rate) {
        super(name, age, gender, id, phoneNumber, email);
        this.hours = hours;
        this.rate = rate;


    }

    @Override
    public String toString() {
        return "Programmer with : {" +
                "name='" + name + '\'' +
                ", gender='" + gender + '\'' +
                ", age=" + age +
                ", id=" + id +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", email='" + email + '\'' +
                ", hours=" + hours +
                ", rate=" + rate +
                '}';
    }




    public int getHours() {
        return hours;
    }

    public void setHours(int hours) {
        this.hours = hours;
    }

    public int getRate() {
        return rate;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }




    @Override
    public String get1Title() {
        String title = " Earns ";
        return title;
    }

    @Override
    public double calcSalary() {
        if ( hours <= 160)
        {
            double salary = hours * rate;
            return salary;
        }
        else if (hours > 160 && hours <= 170)
        {
            double salary = 160 * rate;
            return salary;
        }
        else if (hours > 170)
        {
            double salary = (160 * rate) + (hours - 160) * (rate * 1.5);
            return salary;
        }
        else
        {
            double salary = 0;
            return salary;
        }

    }
}
