import java.io.BufferedReader;
import java.io.FileReader;



public class Series7 {




    public static void main(String[] args) throws Exception {
        BufferedReader inputFile = new BufferedReader(new FileReader("data.txt"));
        String strtmp;
        double value;
        int sum = 0;
        int factor = 0;


        while ((strtmp = inputFile.readLine()) != null) {
            if (!strtmp.isEmpty())
            {
                value = Double.parseDouble(strtmp);
                factor = (int)value;
                sum += factor;
            }

        }


        System.out.println("The arithmetic mean for the given set of numbers equals " + sum);
        inputFile.close();

    }
}
