import java.util.ArrayList;
import java.util.List;

public class OOPSalaries {

    public static void main(String[] args) {

        List<Employee> workers = new ArrayList<Employee>();

        workers.add(new Programmer("Vasya", 28, "Male", 323, "9379992", "Vasya@mail.com",171,30));
        workers.add(new SalesManager("Ignat",23,"Male", 123, "1234567", "Ignat@gmail.com", 1000, 10));
        workers.add(new LeadManager("Nina",32,"Female", 8998, "3957694", "Nina@gmail.com", 0.25, 3000));
        workers.add(new CliningManager("Anna",52,"Female", 8932, "3957456", "Anna@gmail.com",500));
        workers.add(new Accounter("Galina",47,"Female", 0157, "5673489", "Galina@mail.com",800));


        for(int i = 0; i < workers.size(); i++ )
        {
            System.out.print(workers.get(i).toString());
            System.out.print(workers.get(i).get1Title());
            System.out.println(workers.get(i).calcSalary());


        }

    }
}
