import java.io.BufferedReader;
import java.io.FileReader;

public class PalindromNumber {


    public static boolean palindromChecker (double number )
    {
        double K = number / 10;
        int x;
        double y;
        int a;
        int z;
        double w;
        double s;
        int b;
        int c;
        int d;

        //Выясняем состоит ли число из 2 цифр
        if (K < 10)
        {
            System.out.println("Число двухзначное");
            //Gjkexftv gthde. wbahe
            w = Math.floor(K) ;

            y = K - w;
            if ((w * 10) / Math.round(y * 10) == 10)
            {
                return true;
            }
        }

        //Выясняем состоит ли число из 3 цифр
        else if (K >= 10 && K <= 99.9)
        {
            System.out.println("Число трёхзначное");
            //Получаем первую цифру числа
            x = (int)number / 100;
            //Получаем первую и вторую цифру числа
            z = (int)number / 10;
            //Уменьшаем число в 10 раз, 3 цифра отображается в дробной части
            s = number / 10;
            //Обнуляем целую часть и умножаем дробную часть на 10 - получаем третью цифру
            w = Math.round((s - z) * 10);
            //Сравниваем первую и третью цифры на равенство
            if (x == w)
            {
                return true;
            }

        }
        //Выясняем состоит ли число из 4 цифр
        else if (K >= 100.0 && K <= 999.9)
        {
            System.out.println("Число четырёххзначное");
            //Находим первую цифру
            x = (int)number / 1000;

            //Получаем число в виде дроби с 0 вместо первой цифры
            y = (number / 1000) - x;
            //Получаем вторую цифру числа
            a = (int) (y * 10);

            b = (int) number / 100;
            // Третья цифра числа
            z = (int)((number / 100 - b) * 10);

            c = (int) number / 10;
            //Четвертая цифра
            d = (int) Math.round((number / 10 - c) * 10);



            //Сравниваем между собой на равенство первую с четвертую  и вторую с третьей цифрой
            if (x == d && a == z)
            {
                return true;
            }


        }
        else
        {
            return false;
        }


        return false;
    }

    public static void main(String[] args)  {
        double number = 0;
        int N = 0;




        if (palindromChecker(77) == true) {
            System.out.println("Палиндром");
        }
        else
            {
                System.out.println("Не палиндром");
            }
    }
}
