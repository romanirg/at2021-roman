import java.io.BufferedReader;
import java.io.FileReader;



public class Series3 {




    public static void main(String[] args) throws Exception {
        BufferedReader inputFile = new BufferedReader(new FileReader("data.txt"));
        String strtmp;
        double value;
        int counter = 0;
        double sum = 0;


        while ((strtmp = inputFile.readLine()) != null) {
            if (!strtmp.isEmpty())
            {
                value = Double.parseDouble(strtmp);
                sum += value;
                counter++;
            }

        }

        
        System.out.println("The arithmetic mean for the given set of numbers equals " + sum/counter);
        inputFile.close();

    }
}
