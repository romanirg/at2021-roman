
package mathshapestester;

import java.util.ArrayList;
import java.util.List;

public class MyRectangle extends MySquare implements MathShape{


    public MyRectangle(double a, double b) {
        super(a);
        this.b = b;
    }

    public double getB() {
        return b;
    }

    public void setB(double b) {
        this.b = b;
    }


    @Override
    public String toString() {
        return super.toString() + ",{" +
                "b =" + b +
                '}';
    }

    protected double b;

    @Override
    public double calcP() {
        return 2 * (this.a + this.b);
    }

    @Override
    public double calcS() {
        return this.a * this.b;
    }
}

