public class LeadManager extends Person implements Employee{

    private double bonus;
    private double salary;

    public LeadManager(String name, int age, String gender, int id, String phoneNumber, String email, double bonus, double salary) {
        super(name, age, gender, id, phoneNumber, email);
        this.bonus = bonus;
        this.salary = salary;
    }


    public double getBonus() {
        return bonus;
    }

    public void setBonus(double bonus) {
        this.bonus = bonus;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }





    @Override
    public String toString() {
        return "LeadManager with: {" +
                "name='" + name + '\'' +
                ", gender='" + gender + '\'' +
                ", age=" + age +
                ", id=" + id +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", email='" + email + '\'' +
                ", salary='" + salary + '\'' +
                ", bonus='" + bonus + '\'' +
                '}';
    }



    @Override
    public double calcSalary() {
        double Salary = (bonus * salary) + salary;
        return Salary;
    }

    @Override
    public String get1Title() {
        String title = " Earns ";
        return title;
    }




}
