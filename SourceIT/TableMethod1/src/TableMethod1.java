public class TableMethod1
{

    public static void main(String[] args)
    {
        int days = 365;

        String [] monthsNames = new String[] {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};

        int [] daysPerMonth = setDaysPerMonth(isYearLeap(days));

        for ( int i = 1; i <=12; i++)
        {
            String nameOfMonth = monthsNames[i - 1];
            int daysInMonth = daysPerMonth[i - 1];
            System.out.println( nameOfMonth + " contains " + daysInMonth + " days");
        }

    }



    public static int isYearLeap(int days)
    {
        int i;
        if (days == 365)
        {
            i = 1;
        } else if (days == 366)
        {
            i = 2;
        }
        else
        {
            i = 3;
        }

        return i;
    }

    public static int[] setDaysPerMonth (int LeapYearIndex)
    {
        int [] daysQuantity = new int[12];
        if (LeapYearIndex == 1)
        {
             daysQuantity = new int[]{31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
        }
        else if (LeapYearIndex == 2)
        {
            daysQuantity = new int[]{31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
        }
        else if (LeapYearIndex == 3)
        {
            daysQuantity = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        }
        return daysQuantity;
    }


}
