package mathshapestester;

public interface MathShape
{
    double calcS();
    double calcP();
}