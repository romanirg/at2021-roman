package calcf3;
import java.util.Scanner;

public class CalcF3 {
    
    
    
    
    public static boolean isNumberEven(int number) 
    {
        if (number % 2 == 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public static void main(String[] args) {
        
        
        System.out.println("Input an integer ");
       
         Scanner in = new Scanner(System.in);
         int a = in.nextInt();
         if (isNumberEven(a) == true)
         {
             System.out.println("You have inputted an even integer");
         }
         else
         {
             System.out.println("You have inputted an odd integer");
         }
        in.close();

    }

}