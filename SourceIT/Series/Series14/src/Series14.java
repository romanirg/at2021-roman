import java.io.*;

public class Series14 {

    public static void main(String[] args) throws IOException {


        BufferedReader inputFile = new BufferedReader(new FileReader("data.txt"));
        String strtmp2;
        int value;
        int K = 7;
        int counter = 0;


        while ((strtmp2 = inputFile.readLine()) != null ) {
            value = Integer.parseInt(strtmp2);


            if (value > K) {
                counter++;
            }
            else if (value == 0)
            {
                break;
            }
            else {
                counter += 0;
            }

        }

        System.out.println("There are " + counter + " digits that are bigger than 0");
        inputFile.close();

    }
}