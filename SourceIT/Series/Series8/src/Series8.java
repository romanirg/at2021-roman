import java.io.BufferedReader;
import java.io.FileReader;



public class Series8 {




    public static void main(String[] args) throws Exception {
        BufferedReader inputFile = new BufferedReader(new FileReader("data.txt"));
        String strtmp = inputFile.readLine();
        int N = Integer.parseInt(strtmp);
        int value;
        int K = 0;


        for (int i = 1; i <= N; i++) {

            strtmp = inputFile.readLine();
            value = Integer.parseInt(strtmp);
            if (value % 2 == 0)
            {
                K++;
                System.out.println(value);
            }
            else
            {
                K += 0;
            }

        }
        System.out.println("There are " + K + " even numbers ");

        inputFile.close();

    }

}