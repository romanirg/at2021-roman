import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;


public class MainPageTests {

    WebDriver driver;
    private MainPage mainPage;
    private AboutUsPage aboutUsPage;
    private ExpertisePage expertisePage;

    List<WebElement>H2Headers = new ArrayList<>();
    List<String>H2HeadersTexts = new ArrayList<>();


    @Before
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "C:\\driversfortesting\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get("https://envionsoftware.com");
        mainPage = new MainPage(driver);
        expertisePage = new ExpertisePage(driver);
        aboutUsPage = new AboutUsPage(driver);
    }


    @Test
    public void Company_Name_Header_Is_Displayed_expect_pass() throws InterruptedException {
        boolean bool = mainPage.checkPresenseOfElement(mainPage.CompanyNameHeader);
        Assert.assertTrue(bool);
    }

    @Test
    public void Company_Name_Header_Has_Correct_Text_expect_pass() throws InterruptedException {
        String actual = mainPage.getTextOfElement(mainPage.CompanyNameHeader);
        Assert.assertEquals("Envion software",actual);
    }

    @Test
    public void Development_Header_Is_Displayed_expect_pass() throws InterruptedException {
        boolean bool = mainPage.checkPresenseOfElement(mainPage.DevelopmentHeader);
        Assert.assertTrue(bool);
    }

    @Test
    public void Development_Header_Has_Correct_Text_expect_pass() throws InterruptedException {
        String actual = mainPage.getTextOfElement(mainPage.DevelopmentHeader);
        Assert.assertEquals("Development with spirit",actual);
    }

    @Test
    public void Company_Logo_Is_Displayed_expect_pass() throws InterruptedException {
        boolean bool = mainPage.checkPresenseOfElement(mainPage.CompanyLogo);
        Assert.assertTrue(bool);
    }

    @Test
    public void Company_Logo_Has_Correct_Link_expect_pass() throws InterruptedException {
        mainPage.clickElement(mainPage.CompanyLogo);
        String actual = mainPage.getCurrentURL();
        Assert.assertEquals("https://envionsoftware.com/",actual);
    }

    @Test
    public void Expertise_Image_Is_Displayed_expect_pass() throws InterruptedException {
        boolean bool = mainPage.checkPresenseOfElement(mainPage.ExpertiseImage);
        Assert.assertTrue(bool);
    }

    @Test
    public void Expertise_Image_Has_Correct_Dimension_expect_pass() throws InterruptedException {
        Dimension actual = mainPage.getDimensionOfElement(mainPage.ExpertiseImage);
        Dimension expected = new Dimension(350,280);
        Assert.assertEquals(expected,actual);
    }

    @Test
    public void Expertise_Image_Has_Correct_Link_expect_pass() throws InterruptedException {
        mainPage.clickElement(mainPage.gamburgerMenuButton);
        mainPage.clickElement(mainPage.AboutUsButtonFromGamburgerMenu);
        String actual =  mainPage.getCurrentURL();
        Assert.assertEquals(aboutUsPage.AboutUsPageURL,actual);
    }


    @Test
    public void Gamburger_Menu_Button_Is_Displayed_expect_pass() throws InterruptedException {
        boolean bool = mainPage.checkPresenseOfElement(mainPage.gamburgerMenuButton);
        Assert.assertTrue(bool);
    }

    @Test
    public void Gamburger_Menu_Button_Has_Correct_Name_expect_pass() throws InterruptedException {
        String name = mainPage.getTextOfElement(mainPage.gamburgerMenuButton);
        Assert.assertEquals("menu",name);
    }



    @Test
    public void Gamburger_Menu_Is_Displayed_expect_pass() throws InterruptedException {
        mainPage.clickElement(mainPage.gamburgerMenuButton);
        boolean bool = mainPage.checkPresenseOfElement(mainPage.gamburgerMenu);
        Assert.assertTrue(bool);
    }


    @Test
    public void Industry_Header_Is_Displayed_expect_pass() throws InterruptedException {
        mainPage.scrollToElement(1);
        boolean bool = mainPage.checkPresenseOfElement(mainPage.IndustryH2Header);
        Assert.assertTrue(bool);
    }


    @Test
    public void First_Industry_H3_Header_Has_Correct_Text_expect_pass() throws InterruptedException {
       String H3HeaderText = mainPage.getTextOfElement(mainPage.ScrollableSectionFirstH3Header);
        Assert.assertEquals("Healthcare",H3HeaderText);
    }

    @Test
    public void Second_Industry_H3_Header_Has_Correct_Text_expect_pass() throws InterruptedException {
//        mainPage.scrollToElement(1);
       String H3HeaderText = mainPage.getTextOfElement(mainPage.ScrollableSectionSecondH3Header);
        Assert.assertEquals("CRM Systems",H3HeaderText);
    }

    @Test
    public void Third_Industry_H3_Header_Has_Correct_Text_expect_pass() throws InterruptedException {
//        mainPage.scrollToElement(1);
       String H3HeaderText = mainPage.getTextOfElement(mainPage.ScrollableSectionThirdH3Header);
        Assert.assertEquals("Data Science",H3HeaderText);
    }

    @Test
    public void Fourth_Industry_H3_Header_Has_Correct_Text_expect_pass() throws InterruptedException {
//        mainPage.scrollToElement(1);
       String H3HeaderText = mainPage.getTextOfElement(mainPage.ScrollableSectionFourthH3Header);
        Assert.assertEquals("Fitness & Wellness",H3HeaderText);
    }

    @Test
    public void Fifth_Industry_H3_Header_Has_Correct_Text_expect_pass() throws InterruptedException {
 //       mainPage.scrollToElement(1);
       String H3HeaderText = mainPage.getTextOfElement(mainPage.ScrollableSectionFifthH3Header);
        Assert.assertEquals("GPS Navigation",H3HeaderText);
    }

    @Test
    public void Sixth_Industry_H3_Header_Has_Correct_Text_expect_pass() throws InterruptedException {
 //       mainPage.scrollToElement(1);
       String H3HeaderText = mainPage.getTextOfElement(mainPage.ScrollableSectionSixthH3Header);
        Assert.assertEquals("Marketing & Advertising",H3HeaderText);
    }

    @Test
    public void Seventh_Industry_H3_Header_Has_Correct_Text_expect_pass() throws InterruptedException {
 //       mainPage.scrollToElement(1);
       String H3HeaderText = mainPage.getTextOfElement(mainPage.ScrollableSectionSeventhH3Header);
        Assert.assertEquals("Media & Education",H3HeaderText);
    }

    @Test
    public void Eighth_Industry_H3_Header_Has_Correct_Text_expect_pass() throws InterruptedException {
 //       mainPage.scrollToElement(1);
       String H3HeaderText = mainPage.getTextOfElement(mainPage.ScrollableSectionEighthH3Header);
        Assert.assertEquals("Natural Language Processing",H3HeaderText);
    }

    @Test
    public void Ninth_Industry_H3_Header_Has_Correct_Text_expect_pass() throws InterruptedException {
  //      mainPage.scrollToElement(1);
       String H3HeaderText = mainPage.getTextOfElement(mainPage.ScrollableSectionNinthH3Header);
        Assert.assertEquals("Social Media",H3HeaderText);
    }

    @Test
    public void Tenth_Industry_H3_Header_Has_Correct_Text_expect_pass() throws InterruptedException {
 //       mainPage.scrollToElement(1);
       String H3HeaderText = mainPage.getTextOfElement(mainPage.ScrollableSectionTenthH3Header);
        Assert.assertEquals("Travel & Hospitality",H3HeaderText);
    }



    @Test
    public void Switch_to_DataScienceTab_byClick_toNext_expect_pass() throws InterruptedException {
        mainPage.scrollToElement(1);
        mainPage.clickElement(mainPage.ScrollableSectionSecondTabNextDiv);
        mainPage.WebDriverWait(mainPage.ScrollableSectionThirdTabNextDiv);
        boolean boo = mainPage.checkPresenseOfElement(mainPage.ScrollableSectionThirdTabNextDiv);
        Assert.assertEquals(true,boo);
    }


    @Test
    public void Switch_to_HealthcareTab_byClick_toPrevious_expect_pass() throws InterruptedException {
        mainPage.scrollToElement(1);
        mainPage.clickElement(mainPage.ScrollableSectionSecondImage);
        boolean bool = mainPage.checkTextOfScrollableSection(mainPage.ScrollableSectionFirstH3Header);
        Assert.assertTrue(bool);
    }



/*    @Test
    public void Main_Page_Contains_3_H2Headers_expect_pass() throws InterruptedException {
        boolean bool = mainPage.checkH2HeadersQuantity(3);
        Assert.assertTrue(bool);
    }

    @Test
    public void first_H2_Has_Correct_Text_expect_pass() throws InterruptedException {
        boolean bool = mainPage.checkH2HeaderNumberMatchesWithText(1, "Industries");
        Assert.assertTrue(bool);

    }

    @Test
    public void second_H2_Has_Correct_Text_expect_pass() throws InterruptedException {
        boolean bool = mainPage.checkH2HeaderNumberMatchesWithText(2, "Services");
        Assert.assertTrue(bool);
    }

    @Test
    public void third_H2_Header_Has_Correct_Text_expect_pass() throws InterruptedException {
        boolean bool = mainPage.checkH2HeaderNumberMatchesWithText(3, "Our clients");
        Assert.assertTrue(bool);
    }*/


    @Test
    public void Main_Page_Contains_27_H3Headers_expect_pass() throws InterruptedException {
        boolean bool = mainPage.checkAllH3HeadersQuantity(27);
        Assert.assertTrue(bool);
    }

    @Test
    public void IndustriesTab_Contains_10_Original_H3Headers_expect_pass() throws InterruptedException {
        boolean bool = mainPage.check_H3Headers_Quantity_IndustriesTab(10);
        Assert.assertTrue(bool);
    }

    @Test
    public void ServicesTab_Contains_5_Original_H3Headers_expect_pass() throws InterruptedException {
        boolean bool = mainPage.check_H3Headers_Quantity_ServicesTab(5);
        Assert.assertTrue(bool);
    }



    @Test
    public void first_IndustryTab_H3_Has_Correct_Text_expect_pass() throws InterruptedException {
        boolean bool = mainPage.checkIndustriesH3HeaderNumberMatchesWithText(1, "Travel & Hospitality");
        Assert.assertTrue(bool);
    }

    @Test
    public void second_IndustryTab_H3_Has_Correct_Text_expect_pass() throws InterruptedException {
        boolean bool = mainPage.checkIndustriesH3HeaderNumberMatchesWithText(2, "Healthcare");
        Assert.assertTrue(bool);
    }

    @Test
    public void third_IndustryTab_H3_Has_Correct_Text_expect_pass() throws InterruptedException {
        boolean bool = mainPage.checkIndustriesH3HeaderNumberMatchesWithText(3, "CRM Systems");
        Assert.assertTrue(bool);
    }

    @Test
    public void fourth_IndustryTab_H3_Has_Correct_Text_expect_pass() throws InterruptedException {
        boolean bool = mainPage.checkIndustriesH3HeaderNumberMatchesWithText(4, "Data Science");
        Assert.assertTrue(bool);
    }

    @Test
    public void fifth_IndustryTab_H3_Has_Correct_Text_expect_pass() throws InterruptedException {
        boolean bool = mainPage.checkIndustriesH3HeaderNumberMatchesWithText(5, "Fitness & Wellness");
        Assert.assertTrue(bool);
    }

    @Test
    public void sixth_IndustryTab_H3_Has_Correct_Text_expect_pass() throws InterruptedException {
        boolean bool = mainPage.checkIndustriesH3HeaderNumberMatchesWithText(6, "GPS Navigation");
        Assert.assertTrue(bool);
    }

    @Test
    public void seventh_IndustryTab_H3_Has_Correct_Text_expect_pass() throws InterruptedException {
        boolean bool = mainPage.checkIndustriesH3HeaderNumberMatchesWithText(7, "Marketing & Advertising");
        Assert.assertTrue(bool);
    }

    @Test
    public void eighth_IndustryTab_H3_Has_Correct_Text_expect_pass() throws InterruptedException {
        boolean bool = mainPage.checkIndustriesH3HeaderNumberMatchesWithText(8, "Media & Education");
        Assert.assertTrue(bool);
    }

    @Test
    public void ninth_IndustryTab_H3_Has_Correct_Text_expect_pass() throws InterruptedException {
        boolean bool = mainPage.checkIndustriesH3HeaderNumberMatchesWithText(9, "Natural Language Processing");
        Assert.assertTrue(bool);
    }

    @Test
    public void tenth_IndustryTab_H3_Has_Correct_Text_expect_pass() throws InterruptedException {
        boolean bool = mainPage.checkIndustriesH3HeaderNumberMatchesWithText(10, "Social Media");
        Assert.assertTrue(bool);
    }

    @Test
    public void first_Services_H3_Has_Correct_Text_expect_pass() throws InterruptedException {
        boolean bool = mainPage.checkServicesH3HeaderNumberMatchesWithText(1, "Business Analysis");
        Assert.assertTrue(bool);
    }

    @Test
    public void second_Services_H3_Has_Correct_Text_expect_pass() throws InterruptedException {
        boolean bool = mainPage.checkServicesH3HeaderNumberMatchesWithText(2, "Product Design");
        Assert.assertTrue(bool);
    }

    @Test
    public void third_Services_H3_Has_Correct_Text_expect_pass() throws InterruptedException {
        boolean bool = mainPage.checkServicesH3HeaderNumberMatchesWithText(3, "Development");
        Assert.assertTrue(bool);
    }

    @Test
    public void fourth_Services_H3_Has_Correct_Text_expect_pass() throws InterruptedException {
        boolean bool = mainPage.checkServicesH3HeaderNumberMatchesWithText(4, "Quality Assurance");
        Assert.assertTrue(bool);
    }

    @Test
    public void fifth_Services_H3_Has_Correct_Text_expect_pass() throws InterruptedException {
        boolean bool = mainPage.checkServicesH3HeaderNumberMatchesWithText(5, "Support");
        Assert.assertTrue(bool);
    }



    @Test
    public void IndustryTab_Contains_10_Paragraphs_expect_pass() throws InterruptedException {
        boolean bool = mainPage.checkParagraphsQuantity_IndustriesTab(10);
        Assert.assertTrue(bool);
    }

    @Test
    public void paragraph_Of_First_IndustryTab_Has_Correct_Text_expect_pass() throws InterruptedException {
        boolean bool = mainPage.checkParagraphsNumberMatchesWithText_IndustriesTab(1, mainPage.ScrollableHealthcareSectionParagraphText);
        Assert.assertTrue(bool);
    }

    @Test
    public void paragraph_Of_Second_IndustryTab_Has_Correct_Text_expect_pass() throws InterruptedException {
        boolean bool = mainPage.checkParagraphsNumberMatchesWithText_IndustriesTab(2, mainPage.ScrollableCRMSystemsSectionParagraphText);
        Assert.assertTrue(bool);
    }

    @Test
    public void paragraph_Of_Third_IndustryTab_Has_Correct_Text_expect_pass() throws InterruptedException {
        boolean bool = mainPage.checkParagraphsNumberMatchesWithText_IndustriesTab(3, mainPage.ScrollableDataScienceSectionParagraphText);
        Assert.assertTrue(bool);
    }

    @Test
    public void paragraph_Of_Fourth_IndustryTab_Has_Correct_Text_expect_pass() throws InterruptedException {
        boolean bool = mainPage.checkParagraphsNumberMatchesWithText_IndustriesTab(4, mainPage.ScrollableFitnessSectionParagraphText);
        Assert.assertTrue(bool);
    }

    @Test
    public void paragraph_Of_Fifth_IndustryTab_Has_Correct_Text_expect_pass() throws InterruptedException {
        boolean bool = mainPage.checkParagraphsNumberMatchesWithText_IndustriesTab(5, mainPage.ScrollableGPSNavigationSectionParagraphText);
        Assert.assertTrue(bool);
    }

    @Test
    public void paragraph_Of_Sixth_IndustryTab_Has_Correct_Text_expect_pass() throws InterruptedException {
        boolean bool = mainPage.checkParagraphsNumberMatchesWithText_IndustriesTab(6, mainPage.ScrollableMarketingSectionParagraphText);
        Assert.assertTrue(bool);
    }

    @Test
    public void paragraph_Of_Seventh_IndustryTab_Has_Correct_Text_expect_pass() throws InterruptedException {
        boolean bool = mainPage.checkParagraphsNumberMatchesWithText_IndustriesTab(7, mainPage.ScrollableMediaAndEducationSectionParagraphText);
        Assert.assertTrue(bool);
    }

    @Test
    public void paragraph_Of_Eighth_IndustryTab_Has_Correct_Text_expect_pass() throws InterruptedException {
        boolean bool = mainPage.checkParagraphsNumberMatchesWithText_IndustriesTab(8, mainPage.ScrollableNaturalLanguageSectionParagraphText);
        Assert.assertTrue(bool);
    }

    @Test
    public void paragraph_Of_Ninth_IndustryTab_Has_Correct_Text_expect_pass() throws InterruptedException {
        boolean bool = mainPage.checkParagraphsNumberMatchesWithText_IndustriesTab(9, mainPage.ScrollableSocialMediaSectionParagraphText);
        Assert.assertTrue(bool);
    }

    @Test
    public void paragraph_Of_Tenth_IndustryTab_Has_Correct_Text_expect_pass() throws InterruptedException {
        boolean bool = mainPage.checkParagraphsNumberMatchesWithText_IndustriesTab(10, mainPage.ScrollableTravelAndHospitalitySectionParagraphText);
        Assert.assertTrue(bool);
    }

/*    @Test
    public void ServicesTab_Contains_5_Paragraphs_expect_pass() throws InterruptedException {
        int bool = mainPage.checkParagraphsQuantityServicesTab(5);
        Assert.assertEquals(1,bool);
    }

    @Test
    public void paragraph_Of_First_ServicesTab_Has_Correct_Text_expect_pass() throws InterruptedException {
        boolean bool = mainPage.checkParagraphsNumberMatchesWithText_ServicesTab(1, mainPage.ServicesBusinessAnalysisSectionParagraphText);
        Assert.assertTrue(bool);
    }*/



    @Test
    public void Main_Page_Contains_3_H2Headers_expect_pass() throws InterruptedException {
        List<WebElement>H2Headers = new ArrayList<>();
        boolean bool = mainPage.get_WebElements_To_Collection(H2Headers,3);
        Assert.assertTrue(bool);
    }

    @Test
    public void first_H2_Has_Correct_Text_expect_pass() throws InterruptedException {
        boolean bool = mainPage.get_Texts_Of_WebElements_To_Collection(1, "Industries",H2Headers,H2HeadersTexts);
        Assert.assertTrue(bool);

    }

/*    @Test
    public void second_H2_Has_Correct_Text_expect_pass() throws InterruptedException {
        boolean bool = mainPage.checkH2HeaderNumberMatchesWithText(2, "Services");
        Assert.assertTrue(bool);
    }

    @Test
    public void third_H2_Header_Has_Correct_Text_expect_pass() throws InterruptedException {
        boolean bool = mainPage.checkH2HeaderNumberMatchesWithText(3, "Our clients");
        Assert.assertTrue(bool);
    }*/









    @After
    public void tearDown()
    {
        driver.quit();
    }
}
