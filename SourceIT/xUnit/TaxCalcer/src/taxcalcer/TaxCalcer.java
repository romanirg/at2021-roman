
package taxcalcer;


public class TaxCalcer {
    

    
    public static TaxInfo calcTax(double salary)
    {
        
        double pn = salary * 0.18;
        double vn = salary * 0.015;
        double pf1 = salary * 0.02;
        double pf2 = salary * 0.22;
        TaxInfo tax = new TaxInfo();
        
        if(salary < 0)
        {
            tax.taxPn = 0;
            tax.taxVn = 0;
            tax.taxPf1 = 0;
            tax.taxPf2 = 0;
        }
        else
        {
            tax.taxPn = pn;
            tax.taxVn = vn;
            tax.taxPf1 = pf1;
            tax.taxPf2 = pf2;
        }
        return tax;
                
                
    }


    public static void main(String[] args) {
        ; //-1000, 'A', 10010110, 023, 0.23, 1.0.11," ",null,NaN
        if(calcTax(1000).taxPn == 180) 
        {
            System.out.println("Test passed");
        }
        else
        {
            System.out.println("Test failed");
        }
        
        if(calcTax(1000).taxVn == 15) 
        {
            System.out.println("Test passed");
        }
        else
        {
            System.out.println("Test failed");
        }
        
        if(calcTax(1000).taxPf1 == 20) 
        {
            System.out.println("Test passed");
        }
        else
        {
            System.out.println("Test failed");
        }
        
        if(calcTax(1000).taxPf2 == 220) 
        {
            System.out.println("Test passed");
        }
        else
        {
            System.out.println("Test failed");
        }
        
    }
    
}
