public class Person extends Man{

    @Override
    public String toString() {
        return super.toString() + ",{" + "id='" + id + '\'' +
                ", phone number =" + phoneNumber +
                ", email ='" + email + '\'' +
                '}';
    }

    public Person(String name, int age, String gender, int id, String phoneNumber, String email) {
        super(name, age, gender);
        this.id = id;
        this.phoneNumber = phoneNumber;
        this.email = email;
    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    protected int id;
    protected String phoneNumber;
    protected String email;
}
