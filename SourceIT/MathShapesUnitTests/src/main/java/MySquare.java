public class MySquare {
    public MySquare(double a) {
        this.a = a;
    }

    public MySquare() {
    }


    public double getA() {
        return a;
    }


    public void setA(double a) {
        this.a = a;
    }



/*    @Override
    public String toString() {
        return "Square{" + "a=" + a + '}';
    }
*/

    public double a;


    public double calcSquare() {
        double S = a * a;
        return S;
    }
}
