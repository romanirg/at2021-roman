
package mathshapestester;

public  class MyTriangle extends MyRectangle implements  MathShape{

    public MyTriangle(double a, double b , double c) {
        super(a, b);
        this.c = c;
    }

    public double getC() {
        return c;
    }

    public void setC(double c) {
        this.c = c;
    }



    @Override
    public String toString() {
        return super.toString() + ",{" +
                "c =" + c +
                '}';
    }

    

    @Override
    public double calcP() {
        return super.calcP();
    }

    @Override
    public double calcS() {
        return super.calcS();
    }
    
    public double calcGeron(double a , double b , double c)
    {
        return myfunctions.MathFunctions.calcGeron(a , b , c);
    }

    protected double c;
}