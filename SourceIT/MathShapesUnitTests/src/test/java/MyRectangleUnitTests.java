import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MyRectangleUnitTests {

    MyRectangle rectangle = new MyRectangle(2.0, 3.0);




    @Test
    public void GetA_Expect_A_Equals_2() {
        double expectedA = 2.0;
        double actualA = rectangle.getA();
        assertEquals(expectedA, actualA, 0.0);
    }

    @Test
    public void GetB_Expect_B_Equals_3() {
        double expectedB = 3.0;
        double actualB = rectangle.getB();
        assertEquals(expectedB, actualB, 0.0);
    }


    @Test
    public void calcSquare_UsingGetAandGetB_Expect_Pass() {
        double expectedS = 6.0;
        rectangle.getA();
        rectangle.getB();
        assertEquals(expectedS , rectangle.calcSquare() , 0.0);
    }

    @Test
    public void SetA_3_Expect_A_Equals_3() {
        double expectedA = 3.0;
        rectangle.setA(3.0);
        assertEquals(expectedA , rectangle.a , 0.0);
    }

    @Test
    public void SetB_10_Expect_B_Equals_10() {
        double expectedB = 2.0;
        rectangle.setB(2.0);
        assertEquals(expectedB , rectangle.b , 0.0);
    }


    @Test
    public void calcSquare_UsingSetA_Expect_Pass() {
        double expectedS = 6.0;
        rectangle.setA(3.0);
        rectangle.setB(2.0);
        assertEquals(expectedS , rectangle.calcSquare() , 0.0);
    }

    @Test
    public void rectangle_Is_InstanceOf_MyRectangleClass_Expect_Pass() {
        Assert.assertTrue(rectangle instanceof MyRectangle);
    }

    @Test
    public void rectangle_Is_InstanceOf_MySquareClass_Expect_Pass() {
        Assert.assertTrue(rectangle instanceof MySquare);
    }

    @Test
    public void rectangle_IsNot_InstanceOf_MyTriangleClass_Expect_Pass() {
        Assert.assertFalse(rectangle instanceof MyTriangle);
    }
}
