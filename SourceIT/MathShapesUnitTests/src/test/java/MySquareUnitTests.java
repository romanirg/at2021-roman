import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class MySquareUnitTests {



    MySquare square = new MySquare(3.0);


        @Test
        public void GetA_Expect_A_Equals_3() {
            double expectedA = 3.0;
            double actualA = square.getA();
            assertEquals(expectedA, actualA, 0.0);
        }


        @Test
        public void calcSquare_UsingGetA_Expect_Pass() {
            double expectedS = 9.0;
            square.getA();
            assertEquals(expectedS , square.calcSquare() , 0.0);
        }

        @Test
        public void SetA_2_Expect_A_Equals_2() {
            double expectedA = 2.0;
            square.setA(2.0);
            assertEquals(expectedA , square.a , 0.0);
        }


        @Test
        public void calcSquare_UsingSetA_Expect_Pass() {
            double expectedS = 4.0;
            square.setA(2.0);
            assertEquals(expectedS , square.calcSquare() , 0.0);
        }

        @Test
        public void square_Is_InstanceOf_MySquareClass_Expect_Pass() {
            Assert.assertTrue(square instanceof MySquare);
        }

        @Test
        public void square_IsNot_InstanceOf_MyRectangleClass_Expect_Pass() {
            Assert.assertFalse(square instanceof MyRectangle);
        }




    }

