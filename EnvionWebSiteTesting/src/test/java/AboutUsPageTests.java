import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class AboutUsPageTests {

    WebDriver driver;
    private AboutUsPage aboutUsPage;
    private MainPage mainPage;


    @Before
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "C:\\driversfortesting\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get("https://envionsoftware.com");
        aboutUsPage = new AboutUsPage(driver);
        mainPage = new MainPage(driver);

    }


    @Test
    public void About_H3Header_Is_Displayed_expect_pass() throws InterruptedException {
        mainPage.clickElement(mainPage.gamburgerMenuButton);
        mainPage.clickElement(mainPage.AboutUsButtonFromGamburgerMenu);
        boolean bool = aboutUsPage.checkPresenseOElement(aboutUsPage.AboutUsH3Header);
        Assert.assertTrue(bool);
    }


    @Test
    public void Image_Is_Displayed_expect_pass() throws InterruptedException {
        mainPage.clickElement(mainPage.gamburgerMenuButton);
        mainPage.clickElement(mainPage.AboutUsButtonFromGamburgerMenu);
        mainPage.scrollToElement(1);
        boolean bool = aboutUsPage.checkPresenseOElement(aboutUsPage.Image);
        Assert.assertTrue(bool);
    }




    @After
    public void tearDown()
    {
        driver.quit();
    }
}




