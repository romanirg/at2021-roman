
package mathshapestester;

public class MySquare implements MathShape {

    public MySquare(double a) {
        this.a = a;
    }

    public void setA(double a) {
        this.a = a;
    }

    public double getA() {
        return a;
    }

    @Override
    public String toString() {
        return "{" +
                "a=" + a +
                '}';
    }

    protected double a;

    @Override
    public double calcS() {
        return this.a * this.a;
    }

    @Override
    public double calcP() {
        return this.a * 4;
    }

    public void print ()
    {
        System.out.println("Square ");
    }
}
