public class SalesManager extends Person implements Employee {

    private double percent;
    private double baseWage;

    public SalesManager(String name, int age, String gender, int id, String phoneNumber, String email, double baseWage, double percent) {
        super(name, age, gender, id, phoneNumber, email);
        this.percent = percent;
        this.baseWage = baseWage;

    }

    @Override
    public String toString() {
        return "SalesManager with: {" +
                "name='" + name + '\'' +
                ", gender='" + gender + '\'' +
                ", age=" + age +
                ", id=" + id +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", email='" + email + '\'' +
                ", percent=" + percent +
                ", baseWage=" + baseWage +
                '}';
    }


    public double getPercent() {
        return percent;
    }

    public void setPercent(double percent) {
        this.percent = percent;
    }

    public double getBaseWage() {
        return baseWage;
    }

    public void setBaseWage(double baseWage) {
        this.baseWage = baseWage;
    }

    @Override
    public String get1Title() {
        String title = " Earns ";
        return title;
    }

    @Override
    public double calcSalary() {
        double salary = ((percent + 100.0) / 100) * baseWage;
        return salary;
    }
}




