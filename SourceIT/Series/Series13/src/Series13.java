import java.io.*;

public class Series13 {

    public static void main(String[] args) throws IOException {


        BufferedReader inputFile = new BufferedReader(new FileReader("data.txt"));
        String strtmp2;
        int value;
        int sum = 0;


        while ((strtmp2 = inputFile.readLine()) != null ) {
            value = Integer.parseInt(strtmp2);

                if (value > 0 && value % 2 == 0) {
                    sum += value;
                }
                else if (value == 0)
                {
                    break;
                }
                else {
                    sum += 0;
                }

            }

        System.out.println(sum);
        inputFile.close();

    }
}