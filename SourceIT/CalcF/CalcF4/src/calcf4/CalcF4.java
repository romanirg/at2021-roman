package calcf4;
import java.util.Scanner;

public class CalcF4 {
    
    
    
    
    public static boolean firstCondition(int a) 
    {
        if (a > 2)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    public static boolean secondCondition(int b) 
    {
        if (b <= 3)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public static void main(String[] args) {
        
        
        System.out.println("Input number A ");
       
         Scanner in = new Scanner(System.in);
         int x = in.nextInt();
         if (firstCondition(x) == true)
         {
             System.out.println("The value of the integer A corresponds to the requirements");
         }
         else
         {
             System.out.println("ERROR.The value of the integer A doesn't correspond to the requirements");
         }
         System.out.println("Input number B ");
         Scanner in2 = new Scanner(System.in);
         int y = in2.nextInt();
         if (secondCondition(y) == true)
         {
             System.out.println("The value of the integer B corresponds to the requirements");
         }
         else
         {
             System.out.println("ERROR.The value of the integer B doesn't correspond to the requirements");
         }          
         
        in.close();

    }

}