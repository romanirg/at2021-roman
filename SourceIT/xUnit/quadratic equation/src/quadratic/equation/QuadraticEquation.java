
package quadratic.equation;

import java.util.Scanner;

public class QuadraticEquation {


    private static double d;

    public static double CalcDiscriminant(double a, double b, double c) {
        double d =  Math.pow(b, 2) - (4 * a * c);
        return d;
    }

    public static double CalcRoot1(double a, double b, double d) {
        EquationRoots equationData = new EquationRoots();
        equationData.x1 = (-b + (Math.sqrt(d))) / (2 * a);
        return equationData.x1;

    }

    public static double CalcRoot2(double a, double b, double d) {
        EquationRoots equationData = new EquationRoots();
        equationData.x2 = (-b - (Math.sqrt(d))) / (2 * a);
        return equationData.x2;
    }




    public static void main(String[] args) {


        System.out.println("Please enter coefficient A");
        Scanner sn = new Scanner(System.in);
        double i = sn.nextDouble();
        System.out.println("Please enter coefficient B");
        double j = sn.nextDouble();
        System.out.println("Please enter coefficient C");
        double k = sn.nextDouble();
        double Discriminant = CalcDiscriminant(i, j, k);


        if (CalcDiscriminant(i , j, k) > 0) {
            System.out.println("The equation has 2 roots: ");
            System.out.println("The first root is " + CalcRoot1(i , j , Discriminant));
            System.out.println("The second root is " + CalcRoot2(i , j ,Discriminant));
        }
        if (CalcDiscriminant(i , j, k) == 0) {
            System.out.println("The equation has only one real root");
            System.out.println("The root 1 is " + CalcRoot1(i , j ,Discriminant));
            System.out.println("The root 2 is " + CalcRoot2(i , j, Discriminant ));
        }
        if (CalcDiscriminant(i , j, k) < 0) {
            System.out.println("The equation has no real roots");


        }


    }
}
