public class MySquare {
    public MySquare(double a) {
        this.a = a;
    }

    public double getA() {
        return a;
    }

    public void setA(double a) {
        this.a = a;
    }

    @Override
    public String toString() {
        String res = "Square{ a = " + a + " }";
        return res;
    }

    protected double a;


}
