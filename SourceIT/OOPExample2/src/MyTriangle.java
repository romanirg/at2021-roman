public class MyTriangle extends MyRectangle {
    public MyTriangle(double a, double b, double c) {
        super(a, b);
        this.c = c;
    }

    public double getC() {
        return c;
    }

    public void setC(double c) {
        this.c = c;
    }

    @Override
    public String toString() {
        String  res = String.format("%s , c = %s" , super.toString(),c);

        String res2 = "";
        //Вырезаем слово Rectangle из унаследованной строки метода toString()
        for (int i = 0; i < 10; i++)
        {
            char Ch = res.charAt(i);
            if ((Ch >= 'a' && Ch <= 'z') || (Ch >= 'A' && Ch <= 'Z')) {
                res2 = res.substring(i + 1);
                continue;
            }
        }

        //Заменяем в полученной строке Rectangle на Triangle
        String res3 = "Triangle";
        String res4 = "";
        res = res3.concat(res2);
        int length = res.length();


        //Обрезаем строку начиная с запятой
        for (int i = 0; i < length; i++)
        {
            char Ch = res.charAt(i);
            if (Ch == ',') {
                res3 = res.substring(i , length);

            }
        }

        //Вырезаем "}"
        for (int i = 0; i < res.length(); i++)
        {
            char Ch = res.charAt(i);
            if (Ch == '}') {
                res4 = res.substring(i , i + 1);
                res2 = res.substring(0 , i);
            }
        }
        //Приводим строку к ее окончательному виду
        res = res2.concat(res3) + " ";
        String res5 = res.concat(res4);

        return res5;




    }

    private double c;
}
