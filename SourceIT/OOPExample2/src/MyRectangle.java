public class MyRectangle extends MySquare {

    public MyRectangle(double a, double b) {
        super(a); //вызов конструктора родителя
        this.b = b;
    }


    protected double b;


    @Override
    public String toString() 
    {
        String res = String.format("%s , b = %s", super.toString(), b);
        
        String res2 = "";

        //Вырезаем слово Square из унаследованной строки метода toString()
        for (int i = 0; i < 8; i++)
        {
            char Ch = res.charAt(i);
            if ((Ch >= 'a' && Ch <= 'z') || (Ch >= 'A' && Ch <= 'Z'))
            {
                res2 = res.substring(i + 1);
                continue;
            }
        }

        //Заменяем в полученной строке Square на Rectangle
        String res3 = "Rectangle";
        String res4 = "";
        res = res3.concat(res2);
        int length = res.length();


        //Обрезаем строку начиная с запятой
        for (int i = 0; i < length; i++)
        {
            char Ch = res.charAt(i);
            if (Ch == ',')
            {
                res3 = res.substring(i , length);
            }
        }

        //Вырезаем "}"
        for (int i = 0; i < res.length(); i++)
        {
            char Ch = res.charAt(i);
            if (Ch == '}') {
                res4 = res.substring(i , i + 1);
                res2 = res.substring(0 , i);
            }
        }

        //Приводим строку к ее окончательному виду
        res = res2.concat(res3) + " ";
        String res5 = res.concat(res4);

        return res5;
    }

        public void setB ( double b)
        {
            this.b = b;
        }


        public double getB ()
        {
            return b;
        }

}