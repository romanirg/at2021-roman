
package calclf33;


public class CalclF33 {

    
    public static void main(String[] args) {
        
        int a = 8;
        int b = 10;
        int c = 3;
        
        if (a + b > c && a + c > b && b + c > a ) {
            System.out.println("Треугольник со сторонами " + a + ", " + b + " и " + c + " существует");
        }else 
            System.out.println("Треугольник со сторонами " + a + ", " + b + " и " + c + " не существует");
    }
    
}
