public class Accounter extends Person implements Employee{

    private double salary;

    public Accounter(String name, int age, String gender, int id, String phoneNumber, String email, double salary) {
        super(name, age, gender, id, phoneNumber, email);
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "Accounter with : {" +
                "name='" + name + '\'' +
                ", gender='" + gender + '\'' +
                ", age=" + age +
                ", id=" + id +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
    @Override
    public double calcSalary() {
        double Salary = salary;
        return Salary;
    }

    @Override
    public String get1Title() {
        String title = " Earns ";
        return title;
    }

}
