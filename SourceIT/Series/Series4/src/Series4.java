import java.io.BufferedReader;
import java.io.FileReader;



public class Series4 {




    public static void main(String[] args) throws Exception {
        BufferedReader inputFile = new BufferedReader(new FileReader("data.txt"));
        String strtmp;
        int N = 10;
        int value;
        int sum = 0;
        int product = 1;


        while ((strtmp = inputFile.readLine()) != null) {

                if (!strtmp.isEmpty()) {
                    value = Integer.parseInt(strtmp);
                    sum += value;
                    product *= value;
                    N--;
                }
                if (N == 0)
                {
                    break;

                }

            }


        System.out.println("The sum of the given set of numbers equals " + sum);
        System.out.println("The product of the given set of numbers equals " + product);

        inputFile.close();

    }
}
